/var/log/cobbler/cobbler.log {
   missingok
   notifempty
   rotate 4
   weekly
   copytruncate
}

/var/log/cobbler/tasks/*.log {
   weekly
   rotate 0
   missingok
   ifempty
   nocompress
   nocreate
   nomail
}

/var/log/cobbler/install.log {
   missingok
   notifempty
   rotate 4
   weekly
}
