#!/usr/bin/make -f

UPSTREAM_GIT := https://github.com/cobbler/cobbler.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=python_distutils --with python2,systemd

override_dh_auto_build:
	python setup.py build build_cfg --install-scripts=/usr/bin --install-data=/usr --install-lib=/usr/lib

override_dh_auto_clean:
	dh_auto_clean
	rm -f docs/*.gz
	rm -f debian/*.upstart debian/*.service debian/*.init

override_dh_auto_test:
	# TODO: find a way to run unit tests
	echo "Do not run unit tests at this time."
#	sed -i s:%%%REPLACE_ME%%%:$(CURDIR): debian/tests.patch
#	patch -p1 <debian/tests.patch
#	PATH=$$PATH:$(CURDIR)/bin PYTHONPATH=.:$(CURDIR)/cobbler nosetests cobbler/*.py || true
#	patch -p1 -R <debian/tests.patch

override_dh_auto_install:
	python setup.py install -f --optimize=1 --install-layout=deb --root=$(CURDIR)/debian/tmp --prefix=/usr/

override_dh_install:
	dh_install -O--buildsystem=python_distutils

	# These have nothing to do here...
	rm $(CURDIR)/debian/cobbler/etc/init.d/cobblerd
	rm -rf $(CURDIR)/debian/cobbler/usr/share/python

	# Symlink to separatedly packaged jquery / jquery-ui
	ln -s /usr/share/javascript/jquery/jquery.min.js	$(CURDIR)/debian/cobbler-common/var/lib/cobbler/www/cobbler_webui_content/jquery.min.js
	ln -s /usr/share/javascript/jquery-ui/jquery-ui.min.js	$(CURDIR)/debian/cobbler-common/var/lib/cobbler/www/cobbler_webui_content/jquery-ui.min.js

	# Move cobbler folder as cobbler-common
	mv $(CURDIR)/debian/cobbler-common/usr/share/cobbler $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common

	# Move the settings.py so that we can copy it in /etc/cobbler in the
	# postinst of cobbler
	mkdir -p $(CURDIR)/debian/cobbler-web/usr/share/cobbler-web/config
	mv $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common/web/settings.py $(CURDIR)/debian/cobbler-web/usr/share/cobbler-web/config
	mkdir -p $(CURDIR)/debian/cobbler-web/usr/share/cobbler-common/web
	ln -s /etc/cobbler/settings.py $(CURDIR)/debian/cobbler-web/usr/share/cobbler-common/web/settings.py

	# Fix location of stuff in /etc/cobbler
	mkdir -p $(CURDIR)/debian/cobbler-common/usr/share/bash-completion/completions
	mv $(CURDIR)/debian/cobbler-common/etc/cobbler/cobbler_bash $(CURDIR)/debian/cobbler-common/usr/share/bash-completion/completions/cobbler.bash_completion
	set -e ; for i in cobbler_web.conf cobbler.conf cobblerd_rotate cobblerd ; do \
		rm $(CURDIR)/debian/cobbler-common/etc/cobbler/$$i ; \
	done

	# Fix the /etc/cobbler/power folder
	rm $(CURDIR)/debian/cobbler-common/etc/cobbler/power/*
	cp $(CURDIR)/debian/power/* $(CURDIR)/debian/cobbler-common/etc/cobbler/power

	# Fix the /etc/cobbler/pxe folder
	rm $(CURDIR)/debian/cobbler-common/etc/cobbler/pxe/*
	cp $(CURDIR)/debian/pxe/* $(CURDIR)/debian/cobbler-common/etc/cobbler/pxe
	cp $(CURDIR)/debian/power/* $(CURDIR)/debian/cobbler-common/etc/cobbler/pxe

	# Move /etc/cobbler/{users.digest, settings} to /usr/share since we want to use
	# debconf for configuring them.
	mkdir -p $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common/debian-package-etc-templates
	mv $(CURDIR)/debian/cobbler-common/etc/cobbler/users.digest $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common/debian-package-etc-templates
	mv $(CURDIR)/debian/cobbler-common/etc/cobbler/settings $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common/debian-package-etc-templates
	sed -i 's|webdir: @@webroot@@/cobbler|webdir: /var/lib/cobbler/www/cobbler|' $(CURDIR)/debian/cobbler-common/usr/share/cobbler-common/debian-package-etc-templates/settings

	# Fix shipped modules.conf to use dnsmasq by default
	pkgos-fix-config-default $(CURDIR)/debian/cobbler-common/etc/cobbler/modules.conf dns module manage_dnsmasq
	pkgos-fix-config-default $(CURDIR)/debian/cobbler-common/etc/cobbler/modules.conf dhcp module manage_dnsmasq
